package Controller;

import Database.MatchDatabase;
import Model.Pitch;
import Model.Player;
import Model.Team;
import Utils.*;

import java.util.*;


public class CricketGame {
    private String gameId;
    private Map<String, TeamStats> teamStatistics;
    private Map<String, PlayerStats> playerStatistics;
    private Team team1;
    private Team team2;
    private int totalOvers;
    private int tossResult;
    private boolean isFirstInning;
    private int currentOver;
    private int currentBall;
    private int currentPlayer;
    private int currentBowler;
    private Team currentTeamBatting;
    private Team currentTeamBowling;
    private boolean isFinished;
    private Set<String> outPlayer;
    private Pitch pitch;

    public CricketGame(Team team1, Team team2, int totalOvers) {

        this.team1 = team1;
        this.team2 = team2;
        this.totalOvers = totalOvers;
        this.gameId = UUID.randomUUID().toString().split("-")[0];
        pitch = new Pitch();
        currentBall = 0;
        currentOver = 0;
        isFirstInning = true;
        currentPlayer = 0;
        currentBowler = 0;
        outPlayer = new HashSet<>();
        initializeScoreBoard();
    }

    public Team getTeam1() {
        return team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public Map<String, TeamStats> getTeamStatistics() {
        return teamStatistics;
    }

    public Map<String, PlayerStats> getPlayerStatistics() {
        return playerStatistics;
    }

    public String getGameId() {
        return gameId;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public int getCurrentBowler() {
        return currentBowler;
    }

    public Team getCurrentTeamBatting() {
        return currentTeamBatting;
    }

    public Team getCurrentTeamBowling() {
        return currentTeamBowling;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void toss() {

        this.tossResult = GenerateRandomNumber.getRandomNumberGeneratorInstance().getRandomNumberForToss();

        if (this.tossResult == Constants.COIN_HEAD) {
            System.out.println(team1.getName() + " WON BATTING !! " + team2.getName() + " WILL BE BOWLING -_-");
            currentTeamBatting = team1;
            currentTeamBowling = team2;
        } else {
            System.out.println(team2.getName() + " WON BATTING !! " + team1.getName() + " WILL BE BOWLING -_-");
            currentTeamBatting = team2;
            currentTeamBowling = team1;
        }

        ScoreVisualizer.printScoreBoardHeading();

    }

    public void playFullGame() {

        while (this.isFinished != true) {
            playOneBallAndUpdateTheStateOfMatch();
        }
        saveMatchDetailsInDBAndDeclareResult();

    }

    private void saveMatchDetailsInDBAndDeclareResult() {

        int teamOneScore = teamStatistics.get(team1.getId()).getScore();
        int teamTwoScore = teamStatistics.get(team2.getId()).getScore();

        boolean teamOneWinner = false;
        boolean teamTwoWinner = false;

        if (teamOneScore > teamTwoScore) {
            teamOneWinner = true;
        } else if (teamTwoScore > teamOneScore) {
            teamTwoWinner = true;
        }

        MatchDatabase.getMatchDatabaseInstance().insertMatchResult(this.gameId, teamOneScore, teamTwoScore,
                teamStatistics.get(team1.getId()).getNumberOfWicketTaken(),
                teamStatistics.get(team2.getId()).getNumberOfWicketTaken(),
                teamStatistics.get(team1.getId()).getNumberOfWicketLost(),
                teamStatistics.get(team2.getId()).getNumberOfWicketLost(), winnerTeamId, losingTeamId);

        ScoreVisualizer.printFinalStandings(teamStatistics, team1, team2);
        ScoreVisualizer.printWinner(teamStatistics, team1, team2);

        MatchDatabase.getMatchDatabaseInstance().insertTeamResult(this, teamOneWinner, teamTwoWinner);

    }

    private void initializeScoreBoard() {
        this.teamStatistics = new HashMap<>();
        this.playerStatistics = new HashMap<>();

        teamStatistics.put(team1.getId(), new TeamStats());
        teamStatistics.put(team2.getId(), new TeamStats());

        for (int i = 0; i < team1.getPlayers().size(); i++) {
            playerStatistics.put(team1.getPlayers().get(i).getId(), new PlayerStats());
            playerStatistics.put(team2.getPlayers().get(i).getId(), new PlayerStats());
        }
    }

    private int performBatting(Player player) {
        return GenerateRandomNumber.getRandomNumberGeneratorInstance().getRandomNumberForBatting(player);
    }

    private String performBowling() {
        return GenerateRandomNumber.getRandomNumberGeneratorInstance().getRandoNumberForBowling();
    }

    private void storeBallRecordInDB(int battingResult) {
        MatchDatabase.getMatchDatabaseInstance()
                     .addEachBallRecord(this.gameId, currentTeamBatting.getId(), battingResult,
                             pitch.getOnStrike().getId(), isFirstInning, currentOver + 1, currentBall + 1,
                             pitch.getBowler().getId());
    }

    private boolean checkIfTeamAllOut() {
        if (teamStatistics.get(currentTeamBatting.getId()).getNumberOfWicketLost() >=
            currentTeamBatting.getPlayers().size() - 1) {
            return true;
        }
        return false;
    }

    private int playOneBall() {

        int battingResult = performBatting(currentTeamBatting.getPlayers().get(currentPlayer));
        String ballingResult = performBowling();

        storeBallRecordInDB(battingResult);

        if (battingResult == Constants.PLAYER_OUT) {

            teamStatistics.get(currentTeamBatting.getId()).increaseWicketLost();
            ScoreVisualizer.printPlayerStanding(playerStatistics, pitch.getOnStrike());
            teamStatistics.get(currentTeamBowling.getId()).increaseWicketTaken();
            playerStatistics.get(pitch.getBowler().getId()).increaseWicketTaken();

            battingResult = 0;
            currentPlayer++;

            if (checkIfTeamAllOut()) {
                return Constants.TEAM_ALL_OUT;
            }

            outPlayer.add(pitch.getOnStrike().getId());
            Player player = selectNextPlayerToPlay();
            pitch.setOnStrike(player);

        } else {

            if (battingResult == Constants.SIX) {
                playerStatistics.get(pitch.getOnStrike().getId()).increaseTotalNumberOfSix();

            } else if (battingResult == Constants.FOUR) {
                playerStatistics.get(pitch.getOnStrike().getId()).increaseTotalNumberOfFour();
            }

            teamStatistics.get(currentTeamBatting.getId()).increaseScore(battingResult);
            playerStatistics.get(pitch.getOnStrike().getId()).increaseScore(battingResult);

        }

        int totalBallLeft = totalOvers * Constants.TOTAL_BALL_IN_A_OVER -
                            (currentOver * Constants.TOTAL_BALL_IN_A_OVER + currentBall + 1);
        int totalRunLeftToMake = teamStatistics.get(currentTeamBowling.getId()).getScore() -
                                 teamStatistics.get(currentTeamBatting.getId()).getScore();

        ScoreVisualizer.printTeamStanding(teamStatistics, currentTeamBatting, currentOver + 1, pitch, battingResult,
                ballingResult, isFirstInning, totalBallLeft, totalRunLeftToMake);

        // SWAP PLAYERS ON PITCH IF THEY MAKE ODD NUMBERS OF RUN BY RUNNING
        if (battingResult % 2 != 0) {
            pitch.swapPlayer();
        }

        if (!ballingResult.equals(Constants.OK_BALL)) {
            return Constants.INCORRECT_BALL;
        }

        return Constants.CORRECT_BALL;

    }

    public int playOneBallAndUpdateTheStateOfMatch() {

        if (isFinished) {
            saveMatchDetailsInDBAndDeclareResult();
            return Constants.MATCH_FINISHED;
        }

        if (isFirstInning && currentOver == totalOvers) {
            secondInningStarting();
            return Constants.ENDING_FIRST_INNING;
        }

        if (!isFirstInning && currentOver >= totalOvers) {
            isFinished = true;
            return Constants.NO_BALL_LEFT;
        }

        //STARTING OF FIRST INNING
        if (isFirstInning && currentBall == 0 && currentOver == 0) {
            pitch.setPlayersOnPitch(this);
        }

        if (checkIfTeamAllOut()) {
            printTeamAllOut();

            if (isFirstInning) {
                secondInningStarting();
            }
            return Constants.TEAM_ALL_OUT;
        }

        int resultOfPlayingOneBall = playOneBall();
        if (resultOfPlayingOneBall != 0) {
            return Constants.INCORRECT_BALL;
        }

        currentBall++;

        // OVER COMPLETED
        if (currentBall == Constants.TOTAL_BALL_IN_A_OVER) {
            currentOver++;
            pitch.swapPlayer();
            currentBowler++;

            if (currentBowler == currentTeamBowling.getPlayers().size()) {
                currentBowler = 0;
            }

            pitch.setBowler(currentTeamBowling.getPlayers().get(currentBowler));
            ScoreVisualizer.printOverEnded(currentOver);
            currentBall = 0;
        }

        if (checkIfTargetReached()) {
            isFinished = true;
            return Constants.MATCH_FINISHED;
        }

        return Constants.CORRECT_BALL;
    }

    private boolean checkIfTargetReached() {
        if (!isFirstInning && teamStatistics.get(currentTeamBatting.getId()).getScore() >
                              teamStatistics.get(currentTeamBowling.getId()).getScore()) {
            return true;
        }
        return false;
    }

    private void secondInningStarting() {

        ScoreVisualizer.printInningEnding(currentTeamBowling);
        ScoreVisualizer.printScoreBoardHeading();

        Team temp = currentTeamBowling;
        currentTeamBowling = currentTeamBatting;
        currentTeamBatting = temp;

        this.currentPlayer = 0;
        this.currentBall = 0;
        this.currentOver = 0;
        this.isFirstInning = false;
        this.currentBowler = 0;

        pitch.setPlayersOnPitch(this);
    }

    private Player selectNextPlayerToPlay() {

        for (Player player : currentTeamBatting.getPlayers()) {
            if (!outPlayer.contains(player.getId()) && player.getId() != pitch.getOffStrike().getId()) {
                return player;
            }
        }
        return null;
    }

    private void printTeamAllOut() {
        System.out.println(currentTeamBatting.getName() + " ALL OUT :( At " +
                           teamStatistics.get(currentTeamBatting.getId()).getScore());
    }

}
