package Database;

import Controller.CricketGame;
import Model.Player;
import Model.Team;
import Utils.PlayerStats;
import Utils.TeamStats;
import io.github.cdimascio.dotenv.Dotenv;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class MatchDatabase {

    static private MatchDatabase matchDatabase = null;
    static private Connection connection = null;
    static private final String PLAYER_TABLE = "players";
    static private final String TEAM_TABLE = "teams";
    static private final String MATCH_TABLE = "matches";
    static private final String TEAM_RESULT_TABLE = "team_results";
    static private final String PLAYER_RESULT_TABLE = "player_results";
    static private final String EACH_BALL_RECORD_TABLE = "each_ball_record";

    private MatchDatabase() {

    }

    public void initializeDatabase() {
        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

            Dotenv dotenv = null;
            dotenv = Dotenv.configure().load();


            //ADD your database name here.
            String DB_NAME = dotenv.get("DB_NAME");
            // ADD your Database username here.
            String DB_USERNAME = dotenv.get("DB_USERNAME");
            // ADD your Database password here.
            String DB_PASSWORD = dotenv.get("DB_PASSWORD");

            String DB_URL = "jdbc:mysql://localhost:3306/" + DB_NAME;
            connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (Exception e) {
            printError(e, "problem in connecting to database");
        }
    }

    public void addPlayer(Player player, String teamId) {

        try {
            String query = "insert into " + PLAYER_TABLE + "(playerId , teamId , playerName) values (?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, player.getId());
            preparedStatement.setString(2, teamId);
            preparedStatement.setString(3, player.getName());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in adding players");
        }
    }

    public void addTeam(String teamId, String teamName) {
        try {
            String query = "insert into " + TEAM_TABLE + " (teamId , teamName) value (?,?) ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, teamId);
            preparedStatement.setString(2, teamName);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in adding teams");
        }
    }

    public void deleteAllPlayerData() {
        try {
            String query = "delete from " + PLAYER_TABLE;
            PreparedStatement preparedStatement1 = connection.prepareStatement(query);
            preparedStatement1.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in deleting players");
        }
    }

    public void deleteAllTeamData() {
        try {
            String query = "delete from " + TEAM_TABLE;
            PreparedStatement preparedStatement2 = connection.prepareStatement(query);
            preparedStatement2.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in deleting team data");
        }
    }

    public void deleteAllMatchRecords() {
        try {
            String query = "delete from " + EACH_BALL_RECORD_TABLE;
            PreparedStatement preparedStatement2 = connection.prepareStatement(query);
            preparedStatement2.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in deleting each ball record");
        }
    }

    public void deleteAllMatches() {
        try {
            String query = "delete from " + MATCH_TABLE;
            PreparedStatement preparedStatement2 = connection.prepareStatement(query);
            preparedStatement2.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in deleting matches");
        }
    }

    public void deleteAllMatchResults() {
        try {
            String query = "delete from " + TEAM_RESULT_TABLE;
            PreparedStatement preparedStatement2 = connection.prepareStatement(query);
            preparedStatement2.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in deleting team result");
        }
    }

    public void addMatch(String matchId, String teamOneId, String teamTwoId, int overs) {
        try {
            String query = "insert into " + MATCH_TABLE +
                           "(matchId , teamOneId , teamTwoId , noOfOvers ) values (?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, matchId);
            preparedStatement.setString(2, teamOneId);
            preparedStatement.setString(3, teamTwoId);
            preparedStatement.setInt(4, overs);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            printError(e, "problem in adding matches");
        }
    }

    public void addEachBallRecord(String matchId, String teamId, int runScored, String playerId,
                                         boolean isFirstInning, int over, int ball, String bowlerId)
            {
        try {
            String query = "insert into " + EACH_BALL_RECORD_TABLE + " values (?,?,?,?,?,?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, matchId);
            preparedStatement.setString(2, teamId);
            preparedStatement.setInt(3, runScored);
            preparedStatement.setString(4, playerId);
            preparedStatement.setBoolean(5, isFirstInning);
            preparedStatement.setInt(6, over);
            preparedStatement.setInt(7, ball);
            preparedStatement.setString(8, bowlerId);
            preparedStatement.executeUpdate();
        }
        catch (Exception e){
            printError(e , "Error in adding each ball record");
        }


    }



    public void insertTeamResult(CricketGame cricketGame, boolean teamOneResult, boolean teamTwoResult) {

        try {
            String query = "insert into " + TEAM_RESULT_TABLE + " values (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            insertOneTeamRecord(cricketGame.getGameId(), cricketGame.getTeam1().getId(),
                    cricketGame.getTeamStatistics(), teamOneResult, preparedStatement);
            insertOneTeamRecord(cricketGame.getGameId(), cricketGame.getTeam2().getId(),
                    cricketGame.getTeamStatistics(), teamTwoResult, preparedStatement);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            printError(e, "Error adding team result");
        }

    }

    private void insertOneTeamRecord(String matchId, String teamId, Map<String, TeamStats> teamStatsMap,
                                     boolean isWinner, PreparedStatement preparedStatement) throws SQLException{


        preparedStatement.setString(1, matchId);
        preparedStatement.setString(2, teamId);
        preparedStatement.setInt(3, teamStatsMap.get(teamId).getScore());
        preparedStatement.setInt(4, teamStatsMap.get(teamId).getNumberOfWicketLost());
        preparedStatement.setInt(5, teamStatsMap.get(teamId).getNumberOfWicketTaken());
        preparedStatement.setBoolean(6, isWinner);

        preparedStatement.addBatch();

    }


    public void insertPlayerResult(CricketGame match) throws SQLException{

        try {
            String query = "insert into " + PLAYER_RESULT_TABLE + " values(?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            insertOneTeamResult(match.getGameId(), match.getTeam1(), preparedStatement, match.getPlayerStatistics());
            insertOneTeamResult(match.getGameId(), match.getTeam2(), preparedStatement, match.getPlayerStatistics());

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            printError(e, "Error in inserting player result");
        }
    }

    private void insertOneTeamResult(String gameId, Team team, PreparedStatement preparedStatement,
                                     Map<String, PlayerStats> playerStatistics) throws SQLException{

            for (Player player : team.getPlayers()) {

                preparedStatement.setString(1, gameId);
                preparedStatement.setString(2, player.getId());
                preparedStatement.setInt(3, playerStatistics.get(player.getId()).getScore());
                preparedStatement.setInt(4, playerStatistics.get(player.getId()).getTotalNumberOfSix());
                preparedStatement.setInt(5, playerStatistics.get(player.getId()).getTotalNumberOfFour());
                preparedStatement.setInt(6, playerStatistics.get(player.getId()).getNumberOfWicketTaken());

                preparedStatement.addBatch();
            }
    }

    public static MatchDatabase getMatchDatabaseInstance() {
        if (matchDatabase == null) {
            matchDatabase = new MatchDatabase();
            matchDatabase.initializeDatabase();
        }

        return matchDatabase;
    }

    private static void printError(Exception e, String message) {
        System.out.println(e.getMessage());
        System.out.println(message);
    }

}

