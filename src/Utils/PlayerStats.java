package Utils;

public class PlayerStats {

    private int score;
    private int numberOfWicketTaken;

    private int totalNumberOfSix;

    private int getTotalNumberOfFour;

    public PlayerStats setScore(int score) {
        this.score = score;
        return this;
    }

    public int getScore() {
        return this.score;
    }

    public int getNumberOfWicketTaken() {
        return this.numberOfWicketTaken;
    }


    public PlayerStats setNumberOfWicketTaken(int numberOfWicketTaken) {
        this.numberOfWicketTaken = numberOfWicketTaken;
        return this;
    }


    public PlayerStats increaseScore(int score) {
        this.score += score;
        return this;
    }


    public PlayerStats increaseWicketTaken() {
        this.numberOfWicketTaken += 1;
        return this;
    }

    public int getTotalNumberOfSix() {
        return totalNumberOfSix;
    }

    public void increaseTotalNumberOfSix() {
        this.totalNumberOfSix += 1;
    }

    public int getTotalNumberOfFour() {
        return getTotalNumberOfFour;
    }

    public void increaseTotalNumberOfFour() {
        this.getTotalNumberOfFour += 1;
    }

}
