package Utils;

import Model.BatsMan;
import Model.Player;

public class GenerateRandomNumber {

    private static GenerateRandomNumber generateRandomNumber = null;

    //RUNS A PLAYER CAN SCORE WITH PROBABILITY OF OUT LOW
    final static private int[] BATSMAN = { 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 6, 6, Constants.PLAYER_OUT };

    //RUNS PLAYER CAN SCORE WITH PROBABILITY OF OUT HIGHER
    final static private int[] BOWLER = { 0, 0, 1, 1, 2, 2, 3, 4, 6, Constants.PLAYER_OUT };

    final static private int[] TOSS_RESULT = { Constants.COIN_HEAD, Constants.COIN_TAIL };

    //A BALL CAN BE WIDE, OK or No BALL.
    final static private String[] BALLS = { Constants.OK_BALL, Constants.OK_BALL, Constants.OK_BALL, Constants.OK_BALL,
            Constants.WIDE_BALL, Constants.NO_BALL, Constants.OK_BALL };

    private GenerateRandomNumber() {
    }


    public int getRandomNumberForToss() {
        int min = 0, max = 1;
        double a = Math.random() * (max - min + 1) + min;
        return TOSS_RESULT[(int) a];
    }

    public int getRandomNumberForBatting(Player player) {
        int min = 0 , max = 1;
        if (player instanceof BatsMan) {
            max = BATSMAN.length - 1;
        } else {
            max = BOWLER.length - 1;
        }

        double a = Math.random() * (max - min + 1) + min;
        if (player instanceof BatsMan) {
            return BATSMAN[(int) a];
        }
        return BOWLER[(int) a];
    }

    public static GenerateRandomNumber getRandomNumberGeneratorInstance(){
        if(generateRandomNumber == null){
            generateRandomNumber = new GenerateRandomNumber();
        }
        return generateRandomNumber;
    }


    public String getRandoNumberForBowling() {
        int min =0 ,max = BALLS.length - 1;
        double a = Math.random() * (max - min + 1) + min;
        return BALLS[(int) a];
    }


}
