package Utils;

public class Constants {

    public static final int TOTAL_BALL_IN_A_OVER = 6;
    public static final int PLAYER_OUT = -1;
    public static final int COIN_HEAD = 0;
    public static final int COIN_TAIL = 1;
    public static final String OK_BALL = "ok_ball";
    public static final String WIDE_BALL = "wide_ball";
    public static final String NO_BALL = "no_ball";
    public static final int SIX = 6;
    public static final int FOUR = 4;
    public static final int TEAM_ALL_OUT = -1;
    public static final int MATCH_FINISHED = -1;
    public static final int ENDING_FIRST_INNING = -3;
    public static final int NO_BALL_LEFT = -4;
    public static final int INCORRECT_BALL = -1;
    public static final int CORRECT_BALL = 0;

    private Constants() {
    }

}

