package Utils;

import Model.Pitch;
import Model.Player;
import Model.Team;

import java.util.Map;

public class ScoreVisualizer {

    public static void printTeamStanding(Map<String, TeamStats> scoreBoard, Team team, int overDone, Pitch pitch,
                                         int runOnCurrentBall, String ballingResult, boolean isFirstInning,
                                         int totalBallsLeft, int runLeftToMake) {

        System.out.printf("%2s %10s %16s %12s %12d %15d %10d %10d", team.getName(), pitch.getOnStrike().getName(),
                pitch.getOffStrike().getName(), pitch.getBowler().getName(), runOnCurrentBall,
                scoreBoard.get(team.getId()).getScore(), overDone,
                scoreBoard.get(team.getId()).getNumberOfWicketLost());


        if (ballingResult.equals("ok_ball")) {
            ballingResult = "            ";
        }

        System.out.printf("%15s", ballingResult);

        if (runLeftToMake < 0) {
            runLeftToMake = 0;
        }

        if (!isFirstInning) {
            System.out.printf("%10s %2d %10s %2d ", "     run left to make :", runLeftToMake, "  balls left : ",
                    totalBallsLeft);
        }

        System.out.println();

    }

    private static void printLining() {
        System.out.println(
                "-------------------------------------------------------------------------------------------------------");
    }

    public static void printPlayerStanding(Map<String, PlayerStats> scoreBoard, Player player) {

        printLining();
        System.out.println(player.getName() + " OUT AT " + scoreBoard.get(player.getId()).getScore());
        printLining();

    }

    public static void printInningEnding(Team team) {

        System.out.println("----------------------------------------XXXXXXXXXXXX" +
                           "---------------------------------------------------");
        System.out.println(team.getName() + " COMING FOR BATTING :) ");
        System.out.println("----------------------------------------XXXXXXXXXXXX" +
                           "---------------------------------------------------");

    }

    public static void printWinner(Map<String, TeamStats> scoreBoard, Team team1, Team team2) {


        int teamOneScore = scoreBoard.get(team1.getId()).getScore();
        int teamTwoScore = scoreBoard.get(team2.getId()).getScore();

        if (teamOneScore > teamTwoScore) {
            System.out.println(
                    team1.getName().toUpperCase() + " WINS :) " + "By : " + Math.abs(teamOneScore - teamTwoScore) +
                    " runs");

        } else if (teamTwoScore > teamOneScore) {
            System.out.println(
                    team2.getName().toUpperCase() + " WINS :) " + "By : " + Math.abs(teamOneScore - teamTwoScore) +
                    " runs");

        } else {
            System.out.println("DRAW !! Lets Have A Super Over");
        }

    }

    public static void printOverEnded(int over) {
        printLining();
        System.out.println(over + " Over Completed !!");
        printLining();
    }

    public static void printScoreBoardHeading() {
        System.out.println("TeamName  BatsManName  OffStrikerName BowlerName  RunOnCurrentBall  TotalScore  OversDone" +
                           "  WicketsLost ");
    }

    public static void printFinalStandings(Map<String, TeamStats> scoreBoard, Team team1, Team team2) {

        System.out.println("-------------------------------------MATCH " +
                           "ENDED-------------------------------------------------------");
        System.out.println("OVER ALL STANDINGS");
        System.out.println("TeamName    TotalScore   WicketsLost WicketsTaken");
        System.out.printf("%4s %10s %12d %15d", team1.getName(), scoreBoard.get(team1.getId()).getScore(),
                scoreBoard.get(team1.getId()).getNumberOfWicketLost(),
                scoreBoard.get(team1.getId()).getNumberOfWicketTaken());
        System.out.println();
        System.out.printf("%4s %8s %12d %15d", team2.getName(), scoreBoard.get(team2.getId()).getScore(),
                scoreBoard.get(team2.getId()).getNumberOfWicketLost(),
                scoreBoard.get(team2.getId()).getNumberOfWicketTaken());
        System.out.println();

    }
}
