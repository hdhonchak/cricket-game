package Utils;

public class TeamStats {

    int score;
    int numberOfWicketTaken;
    int numberOfWicketLost;


    public int getScore() {
        return this.score;
    }


    public TeamStats setScore(int score) {
        this.score = score;
        return this;
    }


    public int getNumberOfWicketTaken() {
        return this.numberOfWicketTaken;
    }


    public int getNumberOfWicketLost() {
        return this.numberOfWicketLost;
    }


    public TeamStats setNumberOfWicketLost(int numberOfWicketLost) {
        this.numberOfWicketLost = numberOfWicketLost;
        return this;
    }


    public TeamStats increaseScore(int score) {
        this.score += score;
        return this;
    }


    public TeamStats increaseWicketTaken() {
        this.numberOfWicketTaken += 1;
        return this;
    }


    public TeamStats increaseWicketLost() {
        this.numberOfWicketLost += 1;
        return this;
    }
}
