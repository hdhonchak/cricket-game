package Model;

import java.util.UUID;

public class BatsMan implements Player {

    private String id;
    private String name;

    public BatsMan(String name) {
        this.id = UUID.randomUUID().toString().split("-")[0];
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
