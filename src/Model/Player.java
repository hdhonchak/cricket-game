package Model;

public interface Player {

    String getId();

    void setId(String id);

    String getName();

    void setName(String name);

}
