package Model;

import Controller.CricketGame;

public class Pitch {


    private Player onStrike;

    private Player offStrike;

    private Player Bowler;


    public Pitch() {
    }

    public Player getOnStrike() {
        return onStrike;
    }

    public void setOnStrike(Player onStrike) {
        this.onStrike = onStrike;
    }

    public Player getOffStrike() {
        return offStrike;
    }

    public void setOffStrike(Player offStrike) {
        this.offStrike = offStrike;
    }

    public Player getBowler() {
        return Bowler;
    }

    public void setBowler(Player bowler) {
        Bowler = bowler;
    }


    public void swapPlayer() {
        Player p1 = this.onStrike;
        Player p2 = this.offStrike;

        onStrike = p2;
        offStrike = p1;

    }

    public void setPlayersOnPitch(CricketGame cricketGame){
        this.setOnStrike(cricketGame.getCurrentTeamBatting().getPlayers().get(cricketGame.getCurrentPlayer()));
        this.setOffStrike(cricketGame.getCurrentTeamBatting().getPlayers().get(cricketGame.getCurrentPlayer() + 1));
        this.setBowler(cricketGame.getCurrentTeamBowling().getPlayers().get(cricketGame.getCurrentBowler()));
    }

}
