import Controller.CricketGame;
import Database.MatchDatabase;
import Model.BatsMan;
import Model.Bowler;
import Model.Player;
import Model.Team;

import java.sql.SQLException;
import java.util.ArrayList;


public class Application {

    public static void main(String[] args) {

        ArrayList<Player> team1 = new ArrayList<>();
        ArrayList<Player> team2 = new ArrayList<>();


        Team india = new Team("India", team1);
        MatchDatabase.getMatchDatabaseInstance().addTeam(india.getId(), india.getName());
        Team england = new Team("England", team2);
        MatchDatabase.getMatchDatabaseInstance().addTeam(england.getId(), england.getName());


        Player player1 = new BatsMan("Rahul");
        MatchDatabase.getMatchDatabaseInstance().addPlayer(player1, india.getId());
        india.getPlayers().add(player1);


        Player player2 = new Bowler("Sachin");
        MatchDatabase.getMatchDatabaseInstance().addPlayer(player2, india.getId());
        india.getPlayers().add(player2);


        Player player3 = new BatsMan("Harsh");
        MatchDatabase.getMatchDatabaseInstance().addPlayer(player3, india.getId());
        india.getPlayers().add(player3);


        Player player4 = new BatsMan("Ayush");
        MatchDatabase.getMatchDatabaseInstance().addPlayer(player4, england.getId());
        england.getPlayers().add(player4);


        Player player5 = new BatsMan("Prabal");
        MatchDatabase.getMatchDatabaseInstance().addPlayer(player5, england.getId());
        england.getPlayers().add(player5);


        Player player6 = new Bowler("Virat");
        MatchDatabase.getMatchDatabaseInstance().addPlayer(player6, england.getId());
        england.getPlayers().add(player6);


        int overs = 20;

        CricketGame cricketGame = new CricketGame(india, england, overs);
        MatchDatabase.getMatchDatabaseInstance()
                     .addMatch(cricketGame.getGameId(), cricketGame.getTeam1().getId(), cricketGame.getTeam2().getId(),
                             overs);
        cricketGame.toss();
        cricketGame.playOneBallAndUpdateTheStateOfMatch();
        cricketGame.playFullGame();

    }
}